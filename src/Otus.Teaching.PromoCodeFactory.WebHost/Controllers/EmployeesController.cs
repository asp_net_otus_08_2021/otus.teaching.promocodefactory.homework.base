﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IMapper _mapper;

        public EmployeesController(IRepository<Employee> employeeRepository, IMapper mapper)
        {
            _employeeRepository = employeeRepository;
            _mapper = mapper;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns>Список всех сотрудников</returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = _mapper.Map<List<EmployeeShortResponse>>(employees);

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <param name="id">Id сотрудника</param>
        /// <returns>Данные сотрудника</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = _mapper.Map<EmployeeResponse>(employee);

            return employeeModel;
        }

        /// <summary>
        /// Создание сотрудника
        /// </summary>
        /// <param name="employeeRequest">Данные нового сотрудника</param>
        /// <returns>Создан сотрудник с присвоенным Id</returns>
        [HttpPut]
        public async Task<ActionResult<EmployeeShortResponse>> CreateEmployee(EmployeeCreateRequest employeeRequest)
        {
            var employee = MapFromCreateRequest(employeeRequest);
            await CreateEmployee(employee);
            return MapToShortResponse(employee);
        }

        /// <summary>
        /// Обновление сотрудника
        /// </summary>
        /// <param name="employeeRequest"></param>
        /// <returns></returns>
        [HttpPost()]
        public async Task<ActionResult<EmployeeShortResponse>> UpdateEmployee(EmployeeUpdateRequest employeeRequest)
        {
            var existingEmployee = await _employeeRepository.GetByIdAsync(employeeRequest.Id);
            if (existingEmployee == null)
            {
                return null;
            }
            else
            {
                var employee = await UpdateEmployee(existingEmployee, employeeRequest);
                return MapToShortResponse(employee);
            }
        }

        /// <summary>
        /// Удаление сотрудника
        /// </summary>
        /// <param name="id">Id сотрудника</param>
        /// <returns>Сообщение с результатом удаления</returns>
        [HttpDelete("delete/{id:guid}")]
        public async Task<ActionResult<string>> DeleteEmployee(Guid id)
        {
            if (await DeleteEmployeeFromRepository(id))
            {
                return new ActionResult<string>("Сотрудник удалён");
            }
            else
            {
                return new ActionResult<string>("Сотрудник не найден");
            }
        }

        private Employee MapFromCreateRequest(EmployeeCreateRequest employeeRequest)
        {
            return _mapper.Map<Employee>(employeeRequest);
        }

        private Employee MapFromUpdateRequest(EmployeeUpdateRequest employeeRequest)
        {
            return _mapper.Map<Employee>(employeeRequest);
        }

        private async Task<Employee> CreateEmployee(Employee employee)
        {
            return await _employeeRepository.CreateAsync(employee);
        }

        private async Task<Employee> UpdateEmployee(Employee existingExmployee, EmployeeUpdateRequest employeeRequest)
        {
            var employee = MapFromUpdateRequest(employeeRequest);
            employee.Roles = existingExmployee.Roles;
            return await UpdateEmployeeRepository(employee);
        }

        private async Task<Employee> UpdateEmployeeRepository(Employee employee)
        {
            return await _employeeRepository.UpdateAsync(employee);
        }

        private EmployeeShortResponse MapToShortResponse(Employee employee)
        {
            return _mapper.Map<EmployeeShortResponse>(employee);
        }

        private async Task<bool> DeleteEmployeeFromRepository(Guid id)
        {
            return await _employeeRepository.DeleteAsync(id);
        }
    }
}