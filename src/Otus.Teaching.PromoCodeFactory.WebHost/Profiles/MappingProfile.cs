﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Profiles
{
	public class MappingProfile : Profile
	{
		public MappingProfile()
		{
			CreateMap<Role, RoleItemResponse>();

			CreateMap<Employee, EmployeeResponse>()
				.ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.Roles));

			CreateMap<Employee, EmployeeShortResponse>();
			CreateMap<EmployeeCreateRequest, Employee>();
			CreateMap<EmployeeUpdateRequest, Employee>();
		}
	}
}
